Dockerised Django Template
===

[![pipeline status](https://gitlab.com/welserjr/udemy---python-django-dev-to-deployment/badges/master/pipeline.svg)](https://gitlab.com/welserjr/udemy---python-django-dev-to-deployment/commits/master)
[![coverage report](https://gitlab.com/welserjr/udemy---python-django-dev-to-deployment/badges/master/coverage.svg)](https://gitlab.com/welserjr/udemy---python-django-dev-to-deployment/commits/master)

Why Dockerize your Django app? Well, here are three compelling reasons:
* Infrastructure-as-code
* Bring development and production environments as close together as possible ([#10 of the Twelve-Factor App](https://12factor.net/dev-prod-parity))
* Increased productivity

## Docker image contain
* python:3.7-alpine
* pipenv
* postgres:10-alpine
* pgadmin4
* Django >=2.1.3,<2.2.0

## Getting started

Install Django

    $ pip install "django>=2.1.3,<2.2.0"


To get started (replace `myproject` with the name of your app):

    $ django-admin.py startproject --template=https://gitlab.com/welserjr/django-docker/-/archive/master/django-docker-master.zip myproject
    $ cd myproject

This readme file is now in your app's directory. You can delete this top bit and everything that follows is the start of your app's readme.

## {{ project_name }}

Description of {{ project_name }}.

## Running in development

Install Docker for Mac or Windows.

Run the app:

    $ docker-compose up

Your app is now available at http://localhost:8001

PgAdmin4 is now available at http://localhost:8080

## Steps to Connect To PgAdmin4 Database
* Login
    * Email: dev@django.com
    * Password: dev_pass
* Create a Server Connection
    * General: Any name
    * Configuration: 
        * Host: db
        * Username: db_user
        * Password: db_pwd

## Add Django App

    $ docker-compose run app sh -c "python manage.py startapp <app_name>"
 
## Running Tests and Check Code
    
    $ docker-compose run app sh -c "python manage.py test && flake8"
   
## Running Coverage
    
    $ docker-compose run app sh -c "coverage run manage.py test"
    $ docker-compose run app sh -c "coverage report -m" 