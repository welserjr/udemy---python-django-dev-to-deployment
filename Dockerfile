FROM python:3.7-alpine
LABEL maintainer="Welser Jr welser.m.r@gmail.com"

ENV PYTHONUNBUFFERED 1

RUN apk add --update --no-cache postgresql-client git jpeg-dev && \
    apk add --update --no-cache --virtual .tmp-build-deps \
    gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev

RUN mkdir /home/app
COPY . /home/app
WORKDIR /home/app

RUN pip install pipenv
RUN pipenv install --dev --system
RUN apk del .tmp-build-deps

RUN mkdir -p /home/web/media
RUN mkdir -p /home/web/static
RUN adduser -D user
RUN chown -R user:user /home/web/
RUN chmod -R 755 /home/web
USER user