from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'btre.apps.pages'
