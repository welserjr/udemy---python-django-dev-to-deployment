from django.apps import AppConfig


class ContactsConfig(AppConfig):
    name = 'btre.apps.contacts'
