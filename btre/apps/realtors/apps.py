from django.apps import AppConfig


class RealtorsConfig(AppConfig):
    name = 'btre.apps.realtors'
