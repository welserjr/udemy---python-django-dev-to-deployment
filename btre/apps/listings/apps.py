from django.apps import AppConfig


class ListingsConfig(AppConfig):
    name = 'btre.apps.listings'
